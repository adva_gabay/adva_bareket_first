#include "server.h"
int main(int argc, char *argv[])
{
	return (serverConnect());
}

int serverConnect()
{
	WSADATA info;
	char buffer[10];
	struct sockaddr_in dest; /* socket info about the machine connecting to us */
	struct sockaddr_in serv; /* socket info about our server */
	int mysocket;            /* socket used to listen for incoming connections */
	int socksize = sizeof(struct sockaddr_in);
	int consocket = 0;
	int err;

	err = WSAStartup(MAKEWORD(2, 0), &info);
	if (err != 0)
	{
		printf("error-WSAstartup failed: %d\n", err);
		system("PAUSE");
		return (1);
	}

	serv.sin_family = AF_INET;         /* set the type of connection to TCP/IP */
	serv.sin_addr.s_addr = INADDR_ANY; /* set our address to any interface */
	serv.sin_port = htons(PORTNUM);    /* set the server port number */

	mysocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	/* bind serv information to mysocket */
	int result = bind(mysocket, (sockaddr *)&serv, sizeof(serv));
	if (result == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
		closesocket(mysocket);
		WSACleanup();
		return -1;
	}
	/* start listening, allowing a queue of up to 1 pending connection */
	while (listen(mysocket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "error- listening function failed with error :" << WSAGetLastError() << std::endl;
	}


	while (1)
	{
		consocket = accept(mysocket, (struct sockaddr *)&dest, &socksize);
		if (consocket == SOCKET_ERROR)
		{
			closesocket(mysocket);
			WSACleanup();
			return 1;
		}
		std::thread* t1 = new std::thread(threadClient, consocket);
		// creates new thread in order to connect the new client to the server
	}

	system("PAUSE");
	closesocket(mysocket);
	return EXIT_SUCCESS;
}

void threadClient(int consocket)
{
	WSADATA info;
	char buffer[10];
	WSAStartup(MAKEWORD(2, 0), &info);
	recv(consocket, buffer, 10, 0);
	send(consocket, "Accepted", 9, 0);
	closesocket(consocket);
	WSACleanup();
}