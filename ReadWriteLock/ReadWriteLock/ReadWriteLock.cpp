#include "ReadWriteLock.h"

void readFile()
{
	unique_lock<mutex> lock(mu);
	cond.wait(lock, [](){ return !isEmpty || writers == 0; });

	cout << "Hey i am a reader! and im reading now!" << endl;

	//Read file...
	this_thread::sleep_for(chrono::seconds(3));
	readers--;

	//call a function that checks if file is empty
	isEmpty = true; //for now...

	lock.unlock();
	cond.notify_one();

	cout << "Hey i am a reader! and im finishing now!" << endl;
}

void writeFile()
{
	unique_lock<mutex> lock(mu);
	cond.wait(lock, [](){ return readers == 0 || isEmpty; });

	cout << "Hey i am a writer! and im writing now!" << endl;

	//Write to file...
	this_thread::sleep_for(chrono::seconds(3));
	writers--;
	isEmpty = false;
	
	lock.unlock();
	cond.notify_all();

	cout << "Hey i am a writer! and im finishing now!" << endl;
}

int main(int argc, char** argv)
{
	readers++;
	thread reader1(readFile);
	readers++;
	thread reader2(readFile);
	writers++;
	thread writer1(writeFile);
	readers++;
	thread reader3(readFile);
	writers++;
	thread writer2(writeFile);

	reader1.join();
	reader2.join();
	reader3.join();
	writer1.join();
	writer2.join();
	
	system("pause");
	return 0;
}