#include <iostream>
#include <fstream>
#include <mutex>
#include <condition_variable>
#include <thread>

using namespace std;

mutex mu;
bool isEmpty = true;
int writers;
int readers;
condition_variable cond;